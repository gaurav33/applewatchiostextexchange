//
//  ViewController.swift
//  watchToApp
//
//  Created by Gaurav Roy on 06/09/19.
//  Copyright © 2019 Gaurav Roy. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textFromWatch: UILabel!
    
    var wcSession: WCSession!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        wcSession = WCSession.default
        wcSession?.delegate = self
        wcSession?.activate()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print(message["watchMess"] as? String)
        textFromWatch.text = message["watchMess"] as? String
        
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        let message = ["mess":textField.text!]
        wcSession.sendMessage(message, replyHandler: nil, errorHandler: {
            error in print(error.localizedDescription)
        })
    }
    

}

