//
//  InterfaceController.swift
//  watchToAppWatch Extension
//
//  Created by Gaurav Roy on 06/09/19.
//  Copyright © 2019 Gaurav Roy. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var wcSession : WCSession!
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
   
    @IBOutlet weak var messageDisplay: WKInterfaceLabel!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        wcSession = WCSession.default
        wcSession?.delegate = self
        wcSession?.activate()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        messageDisplay.setText(message["mess"] as? String)
    }
    
    
    @IBAction func sendWatchMessage() {
        let message = ["watchMess":"Im good"]
        wcSession.sendMessage(message, replyHandler: nil, errorHandler: {
           error in print(error.localizedDescription)
        })
    }
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
